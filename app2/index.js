const express = require('express');
const mysql = require('mysql');

const app = express();

// MySQL connection configuration
const connection = mysql.createConnection({
  host: 'localhost:3306',
  user: 'root',
  password: '',
  database: 'mysql',
});

// Connect to MySQL
connection.connect((err) => {
  if (err) {
    console.error('Error connecting to MySQL:', err);
    return;
  }
  console.log('Connected to MySQL database.');
});

// Define your routes
app.get('/', (req, res) => {
  // Execute a sample MySQL query
  connection.query('SELECT * FROM your_table', (error, results) => {
    if (error) {
      console.error('Error executing MySQL query:', error);
      res.status(500).send('Error executing MySQL query.');
      return;
    }
    res.send(results);
  });
});

// Start the server
app.listen(3000, () => {
  console.log('The second web application is listening on port 3000.');
});
